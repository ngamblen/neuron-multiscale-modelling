# Single neuron multiscale modelling

This project was run by Michele Mattioni during his PhD project under my supervision. **All the code** is his.

The idea was to develop a detailled computational model of a [medium spiny neuron of the striatum](https://en.wikipedia.org/wiki/Medium_spiny_neuron), comprising a reasonnably realistic 3D structure, an electrical model of the entire neuron, and a biochemical model controlling the synaptic plasticity of glutamtergic synapses. We then ran event-driven hybrid simulations of both models.

The modelling and simulation work relies on two pieces of software: neuronvisio, to visualise the model and simulation results, and TimeScale, the event-driven simulation framework.

# neuronvisio
Neuronvisio is a Graphical User Interface for NEURON simulator environment. 
Features
 - 3D visualization of the model with the possibility to change it runtime
 - Creation of vectors to record any variables present in the section
 - Pylab integration to plot directly the result of the simulation
 - Exploration of the timecourse of any variable among time using a color coded scale
 - the GUI runs in its own thread so it’s possible to use the console (integrated with IPython)
 - automatically download and load models from ModelDB

## Where to find it?
https://pypi.org/project/neuronvisio/

## Cite
Mattioni M., Cohen U., Le Novère N. Neuronvisio: a Graphical User Interface with 3D capabilities for NEURON. _Frontiers in Neuroinformatics_ (2012), 6:20. [doi:10.3389/fninf.2012.00020](https://doi.org/10.3389/fninf.2012.00020)

# TimeScale
TimeScales is used to run a Multiscale Model of the Medium Spiny Neuron of the Neostriatum, integrating electrical signalling with biochemical pathways.

## Where to find it?
https://github.com/mattions/TimeScales

http://michelemattioni.me/TimeScales/

# Cite
Mattioni M., Le Novère N. Integration of biochemical and electrical signaling - multiscale model of the medium spiny neuron of the striatum. _PLoS ONE_ (2013) 8(7): e66811. [doi:10.1371/journal.pone.0066811](https://doi.org/10.1371/journal.pone.0066811)

<img src="Images/Mattioni_neuron.png" alt="Neuron geometry" width="200"/>

<img src="Images/Mattioni-spine.png" alt="Dendritic spine geometry" width="200"/>

<img src="Images/Mattioni-signalling.png" alt="Synaptic plasticity" width="200"/>

<img src="Images/Mattioni_integration.png" alt="Electrical and biochemical integration" width="200"/>
